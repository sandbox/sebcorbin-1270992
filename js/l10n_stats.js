(function ($) {
  Drupal.behaviors.l10nStats = {
    attach:function() {
      // Affect tooltip formatter
      for (chart_id in Drupal.settings.l10n_stats) {
        Drupal.settings.l10n_stats[chart_id].tooltip = {formatter: this.formatter};
        new Highcharts.Chart(Drupal.settings.l10n_stats[chart_id]);
      }
    },

    /**
     * Tooltip formatter
     */
    formatter: function() {
      var diff = '';
      if (this.point.x != 0) {
        var prev = this.series.data[this.point.x - 1].y;
        diff = '<br /><span style="color:' + (prev > this.y ? 'red">-' : 'green">+') + Math.abs(prev - this.y) + '</span>';
      }
      return '<strong>' + this.series.name + '</strong><br />' + this.y + diff;
    }
  }
}(jQuery));
